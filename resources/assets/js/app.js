import Vue from 'vue'
import Vuex from 'vuex'

import VueRouter from 'vue-router'

require('./bootstrap');

const game = require("./game/game.vue");


const routes = [
  {
    path: '/',
    name: 'root',
    component: game
  },
];

Vue.use(Vuex);


const router = new VueRouter({
  routes
});

Vue.use(VueRouter);

import vSelect from 'vue-select';
Vue.component('v-select', vSelect);


import VueDialog from 'vuedialog';

import  {default as plugin, Component as Vuedals, Bus} from 'vuedals';
Vue.use(plugin);

VueDialog.setBus(Bus);

const app = new Vue({
  el: '#app',
  router,
  methods: {
  },
  components: {
    'vuedals': Vuedals,
    'vuedialog': VueDialog
  }
});
