try {
  window.$ = window.jQuery = require('jquery');
} catch (e) {}

require('popper.js');
require('tooltip.js');
require('bootstrap');

window.Vue = require('vue');
require('vue-resource');
window.VueRouter = require('vue-router');