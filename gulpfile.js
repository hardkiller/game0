const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

elixir((mix) => {
    mix.sass('app.scss')
    .copy('node_modules/font-awesome/fonts', 'public/fonts')
    .webpack('./resources/assets/js/app.js');
});
