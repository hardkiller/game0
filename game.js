"use strict";


const express = require('express');
const app =     express();

const bodyParser =  require('body-parser');
const compression = require('compression');

const path =    require('path');


/* express middleware configuration part */
app.use(bodyParser({
  limit: '50mb'
}));

app.use(bodyParser.json({
  limit: '50mb'
}));

app.use(bodyParser.urlencoded({
  extended: true
}));

function shouldCompress(req, res) {
  if (req.headers['x-no-compression']) {
    return false;
  }
  return compression.filter(req, res)
}

app.use(compression({
  filter: shouldCompress
}));


/* application basic routes and configuration */
app.use(express.static(
  path.join(__dirname, "/public")
));

app.listen(3000, "0.0.0.0", function () {
  console.log("NodePoint listening on port 3000!");
});